<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Auth::routes();

Route::get('/registro', 'correspondenciaController@index');

Route::get('/corresp', 'correspondenciaController@getData');

Route::post('/corresp', 'correspondenciaController@saveData');

Route::put('/corresp', 'correspondenciaController@updateData');

Route::delete('/corresp/{FOLIO_DGIFE}', 'correspondenciaController@destroy');