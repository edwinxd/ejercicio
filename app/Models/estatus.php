<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class datos extends Model
{

    protected $primaryKey = 'id';

    protected $fillable = ['description'];
  

    public function datos()
    {
        return $this->hasMany('App\Models\datos');
    }
}
