<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class datos extends Model
{
    protected $table = "datosgrls";

    protected $primaryKey = 'FOLIO_DGIFE';

    protected $fillable = ['NO_OFICIO','REMITENTE_id',
                            'CARGO','ASUNTO','FECHA_OFICIO','OBSERVACIONES','FECHA_RECEPCION'
                            ,'ESTATUS','RESPONSABLE_id','TURNADO_id','CARPETA_id'];


    public $timestamps = false;

    public function turnados()
    {
        return $this->belongsTo('App\Models\turnados');
    }

    public function remitentes()
    {
        return $this->belongsTo('App\Models\remitentes');
    }

    public function carpetas()
    {
        return $this->belongsTo('App\Models\carpetas');
    }

    public function estatus()
    {
        return $this->belongsTo('App\Models\estatus');
    }

    public function responsables()
    {
        return $this->belongsTo('App\Models\responsables');
    }
}
