<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\datos;


class correspondenciaController extends Controller
{
    public function index() {
        return view('correspondencia');
    }

    public function saveData(Request $request)
    {
        $correspondencia = datos::create($request->all());
        return $correspondencia;
    }

    public function updateData(Request $request)
    {
        $correspondencia = datos::find($request->FOLIO_DGIFE);
        $correspondencia->ESTATUS = $request->ESTATUS;
        $correspondencia->save();
        return $correspondencia;
    }

    public function getData()
    {
        $correspondencias = datos::all();
        return $correspondencias;
    }

    public function destroy($FOLIO_DGIFE)
    {
        $correspondencia = datos::find($FOLIO_DGIFE);
        $correspondencia->delete();
    }
}
